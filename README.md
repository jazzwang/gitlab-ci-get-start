# README

This is a sample Gitlab CI project

## 短期目標：學習 Gitlab CI 整合

[TOC]

## 2020-06-18 閱讀 Get Start 文件並於 [gitlab.com](http://gitlab.com) 測試

- 文件 \- [https://docs.gitlab.com/ee/ci/quick_start/](https://docs.gitlab.com/ee/ci/quick_start/)
    
    - 通常一個 Gitlab CI pipeline 包括三個階段
        - build
        - test
        - deploy
    - 但不用三個階段都定義。沒有定義的階段將被忽略。
    - 步驟一：建立一個 `.gitlab-ci.yml`
    - 步驟二：`git commit` 然後 `git push` 就會驅動 CI 測試
- 專案 \- [https://gitlab.com/jazzwang/gitlab-ci-get-start](https://gitlab.com/jazzwang/gitlab-ci-get-start)
    
- 第一次 commit： 用文件上的 Ruby on Rails 測試
    
    ```bash
    ~/git$ git clone https://gitlab.com/jazzwang/gitlab-ci-get-start.git
    ~/git$ cd gitlab-ci-get-start/
    ~/git/gitlab-ci-get-start$ cat > .gitlab-ci.yml <<EOF
    image: "ruby:2.5"
    
    before_script:
    - apt-get update -qq && apt-get install -y -qq sqlite3 libsqlite3-dev nodejs
    - ruby -v
    - which ruby
    - gem install bundler --no-document
    - bundle install --jobs $(nproc)  "${FLAGS[@]}"
    
    rspec:
    script:
    - bundle exec rspec
    
    rubocop:
    script:
    - bundle exec rubocop
    EOF
    ~/git/gitlab-ci-get-start$ git add .gitlab-ci.yml
    ~/git/gitlab-ci-get-start$ git commit -a -m "initial .gitlab-ci.yml from official document"
    [master (root-commit) 0de9df5] initial .gitlab-ci.yml from official document
    1 file changed, 16 insertions(+)
    create mode 100644 .gitlab-ci.yml
    ~/git/gitlab-ci-get-start$ git push
    ```
- 第二次 commit： 試試看 Python Application 跟 Bash 指令
    
    ```
    ~/git/gitlab-ci-get-start$ cat > .gitlab-ci.yml << EOF
    image: "debian:latest"
    
    before_script:
      - apt-get update -qq && apt-get install -y python-pip
      - which python
      - which pip
    
    test:
      script:
        - pip install selenium
        - free -m
        - cat /proc/cpuinfo
        - ifconfig
        - which curl
    EOF
    ~/git/gitlab-ci-get-start$ git commit -a -m "update .gitlab-ci.yml"
    ~/git/gitlab-ci-get-start$ git push
    ```

- 觀察 Gitlab CI 測試結果：都是 Job failed
    
    - 似乎只要有任何一行有錯誤，就會出錯

## 2020-06-19 本機 Gitlab Runner 測試

- 2020-06-19 10:20 延續 Gitlab CI 測試 - *疑問一*：怎麼在提交之前，先在本機測試過呢？
    - [https://docs.gitlab.com/runner/commands/#gitlab-runner-exec](https://docs.gitlab.com/runner/commands/#gitlab-runner-exec)
- 2020-06-19 10:20 安裝 Gitlab Runner
    - 文件：[https://docs.gitlab.com/runner/install/osx.html](https://docs.gitlab.com/runner/install/osx.html)
    - 官方安裝法：
    
        ```bash
        ~$ sudo curl --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-darwin-amd64
        ~$ sudo chmod +x /usr/local/bin/gitlab-runner
        ~$ gitlab-runner register
        ~$ gitlab-runner install
        ~$ gitlab-runner start
        ```
    - macOS Homebrew 安裝法：

        ```sh
        ~$ brew search gitlab runner
        ==> Formulae
        gitlab-runner
        ~$ brew install gitlab-runner
        ~$ brew services start gitlab-runner
        ```

- 2020-06-19 10:39:38 測試 Gitlab Runner Docker executor

    ```
    ~/git/gitlab-ci-get-start$ gitlab-runner exec docker test
    ```

- 2020-06-19 11:14:45 *疑問二*： 怎麼儲存 Gitlab Runner 的 STDOUT？
    - 看樣子 Gitlab.com 介面上可以看到
    - (passed) https://gitlab.com/jazzwang/gitlab-ci-get-start/-/jobs/602321064
    - (failed) https://gitlab.com/jazzwang/gitlab-ci-get-start/-/jobs/600965674
    - Gitlab Runner 因為跑在 Local，所以重新導向 STDOUT / STDERR 就可以存成檔案了。
- 2020-06-19 11:14:45 疑問三：怎麼備份 Gitlab CI 的 setting (Ex. 環境變數)

## 2020-06-20

- 2020-06-20 01:16:27
- https://about.gitlab.com/blog/2015/05/06/why-were-replacing-gitlab-ci-jobs-with-gitlab-ci-dot-yml/
- https://docs.gitlab.com/ee/ci/pipelines/pipeline_architectures.html
- https://docs.gitlab.com/ee/ci/examples/test-and-deploy-python-application-to-heroku.html
- http://amueller.github.io/word_cloud/make_a_release.html